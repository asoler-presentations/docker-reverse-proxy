# Reverse-Proxy

## Willkommen

## Thema

## Ich

## Die Aufgabe

Wir haben mehrere Webseiten, welche wir für die Öffentlichkeit zugänglich machen möchten.

Wir möchten Geld und Resources sparen.

Wir möchten, dass das Ganze am Ende halbwegs professionell aussieht.

## Weitere Ziele

Dabei ist es auch wichtig auf Sicherheit und ein gewisser Grad an Automatisierung zu achten.

Ich bin faul und möchte so wenig selber machen müssen wie möglich.

Warum das alles? Ich betreibe mehrere Webseiten, einige sind privater Natur, andere beruflich.

Für den privaten Seiten wäre mir das Egal, aber die beruflichen müssen schon herhalten. 

Was Privat ist und was nicht, wird vom Gesetzgeber nicht deutlich definiert. 
Im Zweifelsfall gilt eine Seite als gewerblich und muss bestimmte "Auflagen" erfüllen. 
Darüber hinaus sind wir seit einigen Jahren in den Genuss der DSGVO gekommen und das bedeutet 
unter anderem, dass es einfach besser ist, wenn jeglicher Seite mit https abgesichert ist.

Anderer Gründe für diese Wünsche und Ziele betreffen SEO. Heißt, wie eine Seite von 
Suchmaschinen gerankt wird (Schlagwort https, duplicated content, ...).

## Sicherheit

Die Seiten werden mit https gesichert. Das bedeutet, wir brauchen Zertifikate für jedes 
Domain, die wir nutzen wollen.

Verbindungen auf unsichere Seiten werden auf sichere Seiten weitergeleitet. Das heißt, 
wenn man versucht auf dir HTTP Seite zuzugreifen, wird auf die HTTPS Seite weitergeleitet.

Sahnehäubchen: Anfragen auf https://www.my-domain.de werden an https://my-domain.de 
weitergeleitet.


## Automatisierung

Wir müssen Seiten on the fly hinzufügen und entfernen können. Das heißt, das System muss in der 
Lage sein zu erkennen, wann eine neue Seite hinzugefügt oder entfernt wird und die 
entsprechende Anpassungen am System vornehmen damit die Seite erreichbar ist oder mit einem 
entsprechenden Fehler antwortet.

Bei der Nutzung von HTTPS, braucht man Zertifikate, um die Verbindung zu sichern. 
Wir verwenden let's encrypt als Zertifizierungsstelle. Das erlaubt uns kostenlose Zertifikate 
zu verwendet, das einzige Nachteil ist das sie nur 3 Monaten gültig sind.
Deswegen wollen wir das die Generierung und Aktualisierung der Zertifikate automatisch 
durchgeführt wird.

Die Seiten müssen beim neustarten des Server automatisch wieder hochgefahren und zur 
Verfügung stehen.


## Bessere Lösung

Jede Webseite kriegt ein eigener Name und verwendet die Standard-Ports

Vorteile:
- Einer Domäne-Namen pro Seite.
- Seite ist erreichbar ohne zusätzliche Port eingabe.

Nachteil:
- Komplexere Konfiguration

## DNS

- Wir müssen alle Domäne-Namen besitzen, welche wir nutzen möchten.
- Die DNS-Auflösung unsere Domänen muss auf die IP-Adresse unsere Server zeigen.
- Wir brauchen eine feste IP-Adresse für unser Server (Evtl. geht auch myt DynDNS, 
weiss ich aber nicht sicher und die Domainnamen sind dann nicht allzu schön)

## Reverse-Proxy
Braucht jemand eine kurze Erklärung, was ein Proxy ist?

Ein Reverse-Proxy ist ein Server, der vor einem oder mehreren Webservern sitzt und Anfragen 
von Clients abfängt. Dies unterscheidet sich von einem Vorwärts-Proxy, bei dem der Proxy vor 
den Clients sitzt. 

Wenn bei einem Reverse-Proxy Clients Anfragen an den Ursprungsserver einer Website senden, 
werden diese Anfragen am Netzwerkrand durch den Reverse-Proxy-Server abgefangen. Der 
Reverse-Proxy-Server sendet dann Anfragen an den Ursprungsserver und empfängt Antworten vom 
Ursprungsserver, welche er dann and den Client weiterleitet.

User Reverse-Proxy besteht auf drei Container-Anwendungen. Zusätzlich dazu wird jede Web-Seite 
durch einen separaten Container zur Verfügung gestellt.

Schauen wir uns unser Reverse-Proxy an. Er besteht wie gesagt auf drei unterschiedliche 
Container mit unterschiedlichen Aufgaben:
- nginx Container: Web Server, der als Proxy fungiert. Er nimmt alle Verbindungen 
auf port 80 (HTTP) und Port 443 (HTTPS) und kontaktiert dann die notwendige Web-Seite.
- docker-gen Container: Achtet darauf welche Container gestartet und gestoppt werden. 
Generiert bei Bedarf eine neue Konfigurationsdatei für den nginx-Container anhand eines 
templates und startet den neu.
- letsencrypt Container: Achtet beim Starten eines Containers, ob er ein Zertifikat benötigt, 
prüft, ob das Zertifikat bereits vorhanden ist und noch gültig ist. Falls nicht erstellt 
eines Neuen. Prüft regelmäßig die Zertifikate und aktualisiert sie, falls nötig.

## Kommunikation

doker-gen und letsencrypt kommunizieren über unix sockets mit dem docker engine, um auf start 
und stop von Containers zu reagieren.

Gemeinsame volumes werden genutzt, um:
- Zertifikate zu generieren und zu speichern.
- Die Konfiguration für den nginx Server zu aktualisieren.

Die Container beinhalten Umgebungsvariablen, um doker-gen und letsencrypt die 
notwendige Informationen weiterzugeben.
