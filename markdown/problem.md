## Die Aufgabe

Wir möchten auf einem physikalischen Server / VM mehrere Webseiten zur Verfügung stellen.


## Weitere Ziele

- Sicherheit
- Automatisierung


## Sicherheit

- HTTPS
- HTTP ⇾ HTTPS
- WWW ⇾ Non WWW


## Automatisierung

- Seiten on the fly
- Zertifikate
- Neustart
